#!/usr/bin/env python3

# Python code to convert an image to ASCII image.
import sys, random, argparse
import numpy as np
import math
from PIL import Image

import shutil
from glob import glob

import random

from time import sleep

# gray scale level values from:
# http://paulbourke.net/dataformats/asciiart/

# 70 levels of grey
gscale1 = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,\"^`'. "
# 10 levels of grey
gscale2 = '@%#*+=-:. '
# 4 levels of grey
gscale3 = '█▓▒░'
gscale = gscale1


import sys

try:
    import tty, termios
except ImportError:
    # Probably Windows.
    try:
        import msvcrt
    except ImportError:
        # FIXME what to do on other platforms?
        # Just give up here.
        raise ImportError('getch not available')
    else:
        getch = msvcrt.getch
else:
    def getch():
        """getch() -> key character
        Read a single keypress from stdin and return the resulting character.
        Nothing is echoed to the console. This call will block if a keypress
        is not already available, but will not wait for Enter to be pressed.
        If the pressed key was a modifier key, nothing will be detected; if
        it were a special function key, it may return the first character of
        of an escape sequence, leaving additional characters in the buffer.
        """
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

def getAverage(image):

	"""
	Given PIL Image, return average value of grayscale value
	"""
	# get image as numpy array
	im = np.array(image.convert('L'))

	# get shape
	w,h = im.shape

	# get average
	return np.average(im.reshape(w*h))

def covertImageToAscii(fileName, cols, rows, gscale, noAnsi):
	"""
	Given Image and dims (rows, cols) returns an m*n list of Images
	"""

	# Let's store everything here
	aimg = []
	# open image and convert to grayscale
	image = Image.open(fileName)
	image = image.convert("RGB")
	red, green, blue = image.split()

	# store dimensions
	W, H = image.size[0], image.size[1]

	# compute width of tile
	w = W/cols

	if rows<0:
		h = w/0.45
		# compute number of rows
		rows = int(H/h)
	else :
		# compute tile height based on aspect ratio and scale
		h = H/rows

	# check if image size is too small
	if cols > W or rows > H:
		print("Image too small for specified cols!")
		exit(0)

	for j in range(rows):
		y1 = int(j*h)
		y2 = int((j+1)*h)

		# correct last tile
		if j == rows-1:
			y2 = H

		for i in range(cols):
			# crop image to tile
			x1 = int(i*w)
			x2 = int((i+1)*w)

			# correct last tile
			if i == cols-1:
				x2 = W

			# crop image to extract tile
			img_r = red.crop((x1, y1, x2, y2))
			img_g = green.crop((x1, y1, x2, y2))
			img_b = blue.crop((x1, y1, x2, y2))

			# get average luminance
			#avg = int(getAverage(img.convert('L')))
			avg_r = int(getAverage(img_r.convert('L')))
			avg_g = int(getAverage(img_g.convert('L')))
			avg_b = int(getAverage(img_b.convert('L')))
			avg = 255-int((avg_r + avg_g + avg_b)/3)
			max_l=avg_r
			if avg_g>max_l:
				max_l=avg_g
			if avg_b>max_l:
				max_l=avg_b
			avg = 255-max_l
			# look up ascii char
			gsval = gscale[int((avg*(len(gscale)-1))/255)]

			if noAnsi:
				aimg.append(gsval)
			else:
				aimg.append("\033["+str(j+1)+";"+str(i+1)+"H" + "\033[38;2;" + str(avg_r) + ";" + str(avg_g) + ";" + str(avg_b) + "m" + gsval)
	return aimg

def centerPrint(text, noAnsi=False):
	if noAnsi==True:
		print(text)
	else:
		columns = shutil.get_terminal_size().columns
		rows = shutil.get_terminal_size().lines
		print('\033[38;2;255;0;0m')
		print('\033[2J\033['+str(int(rows/2))+';'+str(int(columns/2-(len(text)/2)))+'H' + text, end='', flush=True)
	return True

# main() function
def main():
	# create parser
	descStr = """    _    ____   ____ ___ ___ ____       _       _   
   / \\  / ___| / ___|_ _|_ _|  _ \\ ___ (_)_ __ | |_ 
  / _ \\ \\___ \\| |    | | | || |_) / _ \\| | '_ \\| __|
 / ___ \\ ___) | |___ | | | ||  __/ (_) | | | | | |_ 
/_/   \\_\\____/ \\____|___|___|_|   \\___/|_|_| |_|\\__|
                                                    
Converts images in the current directory to ASCII art."""
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=descStr)
	# add expected arguments
	parser.add_argument('--file', dest='imgFile', required=False)
	parser.add_argument('--out', dest='outFile', required=False)
	parser.add_argument('--cols', dest='cols', required=False)
	parser.add_argument('--noansi', action='store_const', const=True)
	parser.add_argument('--levels', dest='levels', type=int, required=False, default=1)

	# parse args
	args = parser.parse_args()

	if args.levels==2:
		gscale=gscale2
	elif args.levels==3:
		gscale=gscale3
	else:
		gscale=gscale1

	if args.noansi:
		noAnsi=True
	else:
		noAnsi=False

	if args.imgFile:
		slides = []
		slides.append(args.imgFile)
	else:
		slides = glob('*.png')
		slides.extend(glob('*.gif'))
		slides.extend(glob('*.jpg'))
		slides.sort()
		if len(slides)<1:
			centerPrint ('No slides found')
			exit
	ascii_slides=[]

	columns=0
	rows=0

	slide=0

	while True:
		if shutil.get_terminal_size().columns!=columns:
			changes=True
		if shutil.get_terminal_size().lines!=rows:
			changes=True
		if changes==True:
			changes=False
			ascii_slides.clear()
			columns = shutil.get_terminal_size().columns
			rows = shutil.get_terminal_size().lines
			i=0
			centerPrint('CONVERTING', noAnsi)
			for imgFile in slides:
				if noAnsi==False:
					bar='█' * int((i+1) * 9//len(slides))
					print('\033['+str(int(rows/2+1))+';'+str(int(columns/2-5))+'H' + bar, end='', flush=True)
				ascii_slides.append(covertImageToAscii(imgFile, columns, rows, gscale, noAnsi))
				if args.outFile:
					f = open(args.outFile + str(i) + '.ansi', "w")
					f.write("".join(ascii_slides[i]))
					f.close() 
				if noAnsi==False:
					random.shuffle(ascii_slides[i])
				i+=1
		for i in ascii_slides[slide]:
			sleep(1/len(ascii_slides[slide]))
			print (i, end='', flush=True)
		g = getch()
		if g=="\r" or g==" " or g=="n" or g=="N":
			slide+=1
			if slide>=len(slides):
				centerPrint('End of Slides', noAnsi)
				if noAnsi==False:
					print('\033[H')
				print()
				break
		if g=="[" or g=="<":
			slide=0
		if g=="]" or g==">":
			slide=len(slides)-1
		if g=="p" or g=="P":
			slide-=1
			if slide<0:
				slide=0
		if g=="q" or g=="Q":
			centerPrint('Exiting!', noAnsi)
			print('\033[H')
			break
# call main
if __name__ == '__main__':
	main()
